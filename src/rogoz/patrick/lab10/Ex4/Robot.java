package rogoz.patrick.lab10.Ex4;

public class Robot extends Thread {
    boolean destroyed = false;
    int x;
    int y;
    public void setRobotCoord(int x, int y){
        this.x=x;
        this.y=y;
    }
    public void run() {
        while (destroyed == false ) {
            System.out.println(getName() + " Se afla pe pozitia = (" +this.x+","+this.y+")" );
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " destroyed");
    }

}
